package com.jarsilio.android.scrambler

import com.jarsilio.android.scrambler.exceptions.InvalidJpegException
import com.jarsilio.android.scrambler.exceptions.InvalidPngException
import com.jarsilio.android.scrambler.exceptions.UnsupportedFileFormatException
import com.jarsilio.android.scrambler.utils.Utils.byteArrayFromInts
import com.jarsilio.android.scrambler.utils.Utils.toHexString
import okio.buffer
import okio.sink
import okio.source
import timber.log.Timber
import java.io.File
import java.nio.charset.Charset

/**
 * Global configuration for *Scrambled*
 */
object ScramblerConfig {
    /**
     * Delete EXIF headers from non-valid JPEG files.
     *
     * Some camera apps generate JPEG images with invalid EXIF headers.
     * If set to true [processInvalidJpegs] will ignore this and delete the metadata anyway
     * as good as it can.
     */
    var processInvalidJpegs: Boolean = false
}

/**
 * Copy [inputFile] to [outputFile] without metadata. Only JPEG and PNG files are supported.
 *
 * You will need to have permission to read from [inputFile] and to write to [outputFile].
 *
 * @param[inputFile] JPEG or PNG image from which the metadata will be stripped.
 * @param[outputFile] File to write image without metadata.
 */
@Suppress("unused")
@ExperimentalUnsignedTypes
fun stripMetadata(
    inputFile: File,
    outputFile: File,
) {
    when (val imageType = getImageType(inputFile)) {
        ImageType.JPG -> JpegStripper.scramble(inputFile, outputFile)
        ImageType.PNG -> PngStripper.scramble(inputFile, outputFile)
        else -> {
            Timber.e("Only JPEG and PNG images are supported (image is $imageType).")
            throw UnsupportedFileFormatException("Only JPEG and PNG images are supported (image is $imageType).")
        }
    }
}

internal object JpegStripper {
    private val jpegSegmentMarker = 0xFF.toByte()
    private val jpegSkippableSegments =
        byteArrayFromInts(0xFE, 0xE0, 0xE1, 0xE2, 0xE3, 0xE4, 0xE5, 0xE6, 0xE7, 0xE8, 0xE9, 0xEA, 0xEB, 0xEC, 0xED, 0xEE, 0xEF)
    private val jpegStartOfStream = 0xDA.toByte()

    @ExperimentalUnsignedTypes
    fun scramble(
        inputFile: File,
        outputFile: File,
    ) {
        outputFile.sink().buffer().use { sink ->
            inputFile.inputStream().source().buffer().use { source ->
                // This writes the first (empty) start of image segment FFD8 (actually, JPEG allows for segments without payload. This code isn't really (yet?) compatible with that).
                sink.write(source, 2)

                while (!source.exhausted()) {
                    var marker = source.readByte()
                    var segmentType = source.readByte()

                    if (marker != jpegSegmentMarker) {
                        Timber.d(
                            "Invalid JPEG. Expected an FF marker (${"%02x".format(
                                marker,
                            )} != ${"%02x".format(
                                jpegSegmentMarker,
                            )}). Will try to skip bytes until we find a JPEG marker and hope for the best",
                        )
                        if (ScramblerConfig.processInvalidJpegs) {
                            while (marker != jpegSegmentMarker || segmentType == jpegSegmentMarker || segmentType == 0x00.toByte()) {
                                Timber.v("Skipping byte in malformed JPEG file")
                                marker = segmentType
                                segmentType = source.readByte()
                            }
                        } else {
                            throw InvalidJpegException(
                                "Invalid JPEG. Expected an FF marker (${"%02x".format(marker)} != ${"%02x".format(jpegSegmentMarker)})",
                            )
                        }
                    }

                    val size = source.readShort().toUShort()
                    if (size < 2u) {
                        Timber.e("Invalid JPEG: segment ${segmentType.toHexString()} has wrong size: $size (<2)")
                        throw InvalidJpegException("Invalid JPEG: segment ${segmentType.toHexString()} has wrong size: $size (<2)")
                    }

                    if (jpegSkippableSegments.contains(segmentType)) {
                        // Skip all APPn (0xEn) and COM (0xFE) segments (See: https://en.wikipedia.org/wiki/JPEG_Image#Syntax_and_structure)
                        Timber.d("Skipping JPEG segment ${segmentType.toHexString()} (APPn or COM): $size bytes")
                        source.skip((size - 2u).toLong()) // The size counts the 2 bytes of the size itself, and we've already read these
                    } else {
                        sink.writeByte(marker.toInt())
                        sink.writeByte(segmentType.toInt())
                        sink.writeShort(size.toInt())

                        if (segmentType == jpegStartOfStream) {
                            // Hopefully there aren't any other segments after the SOS
                            sink.writeAll(source)
                        } else {
                            sink.write(
                                source,
                                (size - 2u).toLong(),
                            ) // The size counts the 2 bytes of the size itself, and we've already read these
                        }
                    }
                }
            }
        }
    }
}

internal object PngStripper {
    private val pngSignature = byteArrayFromInts(0x89, 0x50, 0x4E, 0x47, 0x0D, 0x0A, 0x1A, 0x0A)
    private val pngCriticalChunks = listOf("IHDR", "PLTE", "IDAT", "IEND")

    @ExperimentalUnsignedTypes
    fun scramble(
        inputFile: File,
        outputFile: File,
    ) {
        outputFile.sink().buffer().use { sink ->
            inputFile.inputStream().source().buffer().use { source ->
                val byteArray = source.readByteArray(8)
                if (byteArray contentEquals pngSignature) {
                    sink.write(byteArray)

                    while (!source.exhausted()) {
                        val chunkLength = source.readInt().toUInt()
                        val chunkName = source.readString(4, Charset.forName("ASCII"))
                        val chunkData = source.readByteArray(chunkLength.toLong())
                        val chunkCrc = source.readByteArray(4)

                        if (pngCriticalChunks.contains(chunkName)) {
                            // Only write chunk to scrambled png file if it's one of the four critical chunks (see https://en.wikipedia.org/wiki/Portable_Network_Graphics#Critical_chunks)
                            sink.writeInt(chunkLength.toInt())
                            sink.writeString(chunkName, Charset.forName("ASCII"))
                            sink.write(chunkData)
                            sink.write(chunkCrc)
                        }

                        if (chunkName == "IEND") {
                            // Stop writing after IEND. This could even be malicious and is definitely *not* necessary! And incorrect.
                            break
                        }
                    }
                } else {
                    throw InvalidPngException("Invalid PNG file ($inputFile). It doesn't start with a PNG SIGNATURE.")
                }
            }
        }
    }
}
