package com.jarsilio.android.scrambler.exceptions

import java.io.IOException

open class StripException(message: String) : IOException(message)

class UnsupportedFileFormatException(message: String) : StripException(message)

class InvalidPngException(message: String) : StripException(message)

class InvalidJpegException(message: String) : StripException(message)

class UnexpectedStripException(message: String) : StripException(message)
