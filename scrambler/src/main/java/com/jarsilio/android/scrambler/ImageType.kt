package com.jarsilio.android.scrambler

import com.jarsilio.android.scrambler.utils.Utils.toHexString
import okio.buffer
import okio.source
import timber.log.Timber
import java.io.File
import java.io.FileInputStream
import java.io.FileNotFoundException
import java.io.IOException
import java.io.InputStream

enum class ImageType {
    JPG,
    PNG,
    BMP,
    GIF,
    TIFF,
    UNKNOWN,
}

fun getImageType(file: File): ImageType {
    Timber.d("Getting ImageType from file $file...")
    return try {
        val inputStream = FileInputStream(file)
        getImageType(inputStream)
    } catch (e: FileNotFoundException) {
        Timber.e(e, "Couldn't open input stream from content resolver for file $file")
        ImageType.UNKNOWN
    }
}

private fun getImageType(inputStream: InputStream?): ImageType {
    val magicNumbers = getMagicNumbers(inputStream!!)

    val imageType: ImageType

    if (magicNumbers.startsWith("FFD8")) {
        imageType = ImageType.JPG
        Timber.d("It's a JPEG image")
    } else if (magicNumbers.startsWith("89504E470D0A1A0A")) {
        imageType = ImageType.PNG
        Timber.d("It's a PNG image")
    } else if (magicNumbers.startsWith("424D")) {
        imageType = ImageType.BMP
        Timber.d("It's a BMP image")
    } else if (magicNumbers.startsWith("474946383961") || magicNumbers.startsWith("474946383761")) {
        imageType = ImageType.GIF
        Timber.d("It's a GIF image")
    } else if (magicNumbers.startsWith("49492A00") || magicNumbers.startsWith("4D4D002A")) {
        imageType = ImageType.TIFF
        Timber.d("It's a TIFF image")
    } else {
        imageType = ImageType.UNKNOWN
        Timber.d("It's (probably) not an image. Failed to recognize type.")
    }

    return imageType
}

private fun getMagicNumbers(inputStream: InputStream): String {
    inputStream.source().buffer().use { source ->
        return try {
            val magicBytes = source.readByteArray(8).toHexString()
            Timber.d("First bytes: $magicBytes")
            magicBytes
        } catch (e: IOException) {
            Timber.e(e, "An error occurred while trying to read the file. Supposing it is not an image")
            ""
        }
    }
}
