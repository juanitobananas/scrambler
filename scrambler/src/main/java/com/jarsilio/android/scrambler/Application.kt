package com.jarsilio.android.scrambler

import android.app.Application
import com.jarsilio.android.scrambler.logging.Loggers
import timber.log.Timber

class Application : Application() {
    override fun onCreate() {
        super.onCreate()
        if (BuildConfig.DEBUG) {
            Timber.plant(Loggers.LongTagTree(applicationContext))
        }
    }
}
