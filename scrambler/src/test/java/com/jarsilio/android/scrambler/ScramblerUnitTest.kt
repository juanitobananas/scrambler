package com.jarsilio.android.scrambler

import org.junit.Assert.assertTrue
import org.junit.Test
import java.io.File
import java.nio.file.Files
import java.nio.file.Path

class ScramblerUnitTest {
    private fun isFileEqual(
        firstFile: Path,
        secondFile: Path,
    ): Boolean {
        if (Files.size(firstFile) != Files.size(secondFile)) {
            return false
        }
        // TODO: compare byte by byte to avoid loading the complete image in memory
        val first = Files.readAllBytes(firstFile)
        val second = Files.readAllBytes(secondFile)
        return first.contentEquals(second)
    }

    @ExperimentalUnsignedTypes
    @Test
    fun testScrambleJpeg() {
        val unscrambledFile = File(javaClass.getResource("/marvincito.jpg")!!.path)
        val expectedScrambledFile = File(javaClass.getResource("/marvincito-scrambled.jpg")!!.path)
        val scrambledFile = Files.createTempFile("scrambler-", ".jpg").toFile()
        stripMetadata(unscrambledFile, scrambledFile)
        assertTrue(isFileEqual(expectedScrambledFile.toPath(), scrambledFile.toPath()))
        Files.delete(scrambledFile.toPath())
    }

    @ExperimentalUnsignedTypes
    @Test
    fun testScramblePng() {
        val unscrambledFile = File(javaClass.getResource("/marvincito.png")!!.path)
        val expectedScrambledFile = File(javaClass.getResource("/marvincito-scrambled.png")!!.path)
        val scrambledFile = Files.createTempFile("scrambler-", ".png").toFile()
        stripMetadata(unscrambledFile, scrambledFile)
        assertTrue(isFileEqual(expectedScrambledFile.toPath(), scrambledFile.toPath()))
        Files.delete(scrambledFile.toPath())
    }
}
